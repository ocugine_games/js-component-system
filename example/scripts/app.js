//====================================================================
//  JS Component based Application Logic
//====================================================================
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Example Module
//====================================================================
class ExampleModule extends AppModule{
    // Module Constructor
    constructor(settings = {}) {
        super(settings);
    }

    // Init Module
    initModule(name = "Username"){
        alert("Hello, "+name+"!");
        return this;
    }
}
class ExampleComponent extends AF_Component{
    // Module Constructor
    constructor(settings = {}) {
        super(settings);
    }

    // Test Component
    testComponent(new_name){
        alert("This is component test. Here we call parrent object method");
        this.target.initModule(new_name);
        return this;
    }
}

//====================================================================
//  Initialize Application
//====================================================================
// Initialize Application
let SampleModule = null, SampleComponent = null;
AppFramework.onReady(function(){
    // Initialize App Framework
    let APP = new AppFramework({
        /* Here you can pass any application args. From object you can get them by "this.core.settings" */
    }, true);

    // Add Modules
    APP.loadModule("sample", ExampleModule, {
        /* Module Settings */
    });
    SampleModule = APP.module("sample");

    // Add Components
    SampleModule.addComponent("myComponent", ExampleComponent, {
        /* Component Settings */
    }, function(){
        alert("Component Already Initialized!");
    });
    SampleComponent = SampleModule.getComponent("myComponent");
});

// Test Button 1
function testMe(){
    SampleModule.initModule("John");
}

// Test Button 2
function testMe2(){
    SampleComponent.testComponent("Dave");
}