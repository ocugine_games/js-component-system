//====================================================================
//  JavaScript Component System
//====================================================================
//  (c) 2020 Ocugine Games. Developed by Elijah Rastorguev
//  Semicolon Engine may be freely distributed under the MIT license or
//  GPLv2 License.
//
//  For all details and documentation:
//  https://gitlab.com/ocugine_games/js-component-system/
//
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Core Class Library
//====================================================================
let AF_Instance = null;   // Core Class Instance
class AppFramework{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - App Settings
    //  @for            AppFramework
    //======================================================
    constructor(settings, debug) {
        //*************************************************
        //  PRIVATE CONSTRUCTOR AREA
        //*************************************************
        // Console Styles
        this.debug_styles = [
            'background: red',
            'background: orange',
            'background: gold',
            'background: yellowgreen',
            'background: skyblue',
            'background: steelblue',
            'background: darkviolet',
            'background: white'
        ];

        // Add Object Params
        this.uniqueID = 0;              // Unique ID
        this.modules = {};              // Modules

        //*************************************************
        //  PUBLIC CONSTRUCTOR AREA
        //*************************************************
        // Global Params
        this.debug = false;             // Debug Mode by Default = false
        this.settings = {               // Default Settings
        };

        // Override Default Settings
        if(!this.isUndefined(settings) && this.isObject(settings)) this.configure(settings);
        if(!this.isUndefined(debug)) this.debug = debug; // Set Debug Mode

        //*************************************************
        //  INITIALIZATION AREA
        //*************************************************
        // Set Core Instance
        AF_Instance = this;

        // Show Welcome Message
        this.log("Welcome to the JS Component Engine!", 1, 2);
    }

    //======================================================
    //  @method         getInstance
    //  @usage          Get Instance
    //  @returns        {Object} instance - Core Instance
    //  @for            AppFramework
    //======================================================
    static getInstance(){
        return AF_Instance;
    }

    //======================================================
    //  @method         loadModule
    //  @usage          Load module to Engine
    //  @params         {String} name - Module Name
    //  @params         {Class} class_name - Module Class
    //  @params         {Object} args - Module Arguments
    //  @returns        {Object} instance - Engine Instance
    //  @for            AppFramework
    //======================================================
    loadModule(name, class_name = AF_Base, args = {}){
        let _self = this; // Self Module

        // Check Params
        if(_self.isUndefined(name) || !_self.isString(name)) throw "Failed to load module. Module name must be a string";

        // Check if Module Exists
        if(!_self.isUndefined(_self.modules[name]) && _self.isObject(_self.modules[name])) return _self;
        _self.modules[name] = new class_name(args); // Create Module
        return _self;
    }

    //======================================================
    //  @method         module
    //  @usage          Get Module Instance
    //  @params         {String} Name - Module Name
    //  @returns        {Object} instance - Module Instance
    //                  or false if module is not loaded
    //  @for            AppFramework
    //======================================================
    module(name){
        let _self = this;
        if(!_self.isUndefined(_self.modules[name]) && _self.isObject(_self.modules[name])){
            return _self.modules[name];
        }else{
            return false;
        }
    }

    //======================================================
    //  @method         getModuleByType
    //  @usage          Get Module by Type
    //  @params         {Class} class_name - Module Type
    //  @returns        {Object} instance - Module Instance
    //                  or false if module is not loaded
    //  @for            AppFramework
    //======================================================
    getModuleByType(class_name){
        let _self = this; // Self Module
        let _mods = _self.keys(_self.modules);
        for(let i = 0; i < _mods.length; i++){
            if(_self.modules[_mods[i]] instanceof class_name){
                return _self.modules[_mods[i]];
            }
        }

        // No Module Found
        return false;
    }

    //======================================================
    //  @method         log
    //  @usage          Write Log (Only in Debug Mode)
    //  @params         {String} message
    //  @params         {Integer} color_index1
    //  @params         {Integer} color_index2
    //  @for            AppFramework
    //======================================================
    log(message, color_index1 = 4, color_index2 = 7){
        let _self = this; let _styles = _self.debug_styles;
        if(_self.debug){ // Only in Debug Mode
            console.log('%c App Framework Debug: %c '+message, _styles[color_index1], _styles[color_index2]);
        }
    }

    //======================================================
    //  @method         onReady
    //  @usage          On Document ready to work with Framework
    //  @params         {Function} callback - Callback Function
    //  @for            AppFramework
    //======================================================
    static onReady(callback = function(){}){
        document.addEventListener("DOMContentLoaded", function(event) {
            callback();
        });
    }

    //======================================================
    //  @method         configure
    //  @usage          Configure Framework Core Object
    //  @params         {Object} settings - New Engine Settings
    //  @returns        {Object} instance - Engine Instance
    //  @for            AppFramework
    //======================================================
    configure(settings = {}){
        let _self = this;
        let _newSettings = (!_self.isUndefined(settings) && _self.isObject(settings))?settings:{};
        _self.settings = _self.extendObject(_self.settings, _newSettings);
        return _self;
    }

    //******************************************************
    //  Internal Methods
    //******************************************************
    //======================================================
    //  @method         normalizeArg
    //  @usage          Normalize Arguments from comma-separated
    //                  string.
    //
    //                  For Example: "Arg0,Arg1" generates:
    //                  ["Arg0", "Arg1"]
    //
    //  @params         {String} arg - Arguments to convert
    //  @returns        {Array} args - Array of Arguments
    //  @for            AppFramework
    //======================================================
    normalizeArg(arg) {
        let _self = this;
        if(_self.isString(arg)) {
            arg = arg.replace(/\s+/g,'').split(",");
        }
        if(!_self.isArray(arg)) {
            arg = [ arg ];
        }
        return arg;
    }

    //======================================================
    //  @method         extendObject
    //  @usage          Extend Object by another object
    //  @params         {Object} dest - Destination Object
    //                  {Object} source - Source Object
    //  @returns        {Object} dest - Extended Destination Object
    //  @for            AppFramework
    //======================================================
    extendObject(dest,source) {
        if(!source) { return dest; }
        for (let prop in source) {
            dest[prop] = source[prop];
        }
        return dest;
    }

    //======================================================
    //  @method         cloneObject
    //  @usage          Clone Object
    //  @params         {Object} dest - Destination Object
    //  @returns        {Object} dest - Cloned Destination Object
    //  @for            AppFramework
    //======================================================
    cloneObject(obj) {
        let _self = this;
        return _self.extendObject({},obj);
    }

    //======================================================
    //  @method         hasProperty
    //  @usage          Check if Object has some property
    //  @params         {Object} dest - Destination Object
    //  @params         {String} key - Property Name
    //  @returns        {Bool} Property Exists Flag
    //  @for            AppFramework
    //======================================================
    hasProperty(obj, key) {
        return Object.prototype.hasOwnProperty.call(obj, key);
    }

    //======================================================
    //  @method         isString
    //  @usage          Check if something is a string
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isString(obj) {
        return typeof obj === "string";
    }

    //======================================================
    //  @method         isNumber
    //  @usage          Check if something is a number
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isNumber(obj) {
        return Object.prototype.toString.call(obj) === '[object Number]';
    }

    //======================================================
    //  @method         isFunction
    //  @usage          Check if something is a function
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isFunction(obj) {
        return Object.prototype.toString.call(obj) === '[object Function]';
    }

    //======================================================
    //  @method         isObject
    //  @usage          Check if something is an Object
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isObject(obj) {
        return Object.prototype.toString.call(obj) === '[object Object]';
    };

    //======================================================
    //  @method         isArray
    //  @usage          Check if something is an Array
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    //======================================================
    //  @method         isUndefined
    //  @usage          Check if something is undefined
    //  @params         {Object} dest - Destination Object
    //  @returns        {Bool} Check Status
    //  @for            AppFramework
    //======================================================
    isUndefined(obj) {
        return obj === void 0;
    }

    //======================================================
    //  @method         popProperty
    //  @usage          Removes a property from an object
    //                  and returns it if it exists
    //  @params         {Object} dest - Destination Object
    //  @params         {String} property - Object Property
    //  @returns        {Object} Property or Undefined
    //  @for            AppFramework
    //======================================================
    popProperty(obj,property) {
        let val = obj[property];
        delete obj[property];
        return val;
    }

    //======================================================
    //  @method         each
    //  @usage          Fast Basic Iteration Method
    //  @params         {Object} obj - Object to Each
    //  @params         {Object} iterator - Object Iterator
    //  @params         {Object} context - Object Context
    //  @for            AppFramework
    //======================================================
    each(obj,iterator,context) {
        if (obj == null) { return; }
        if (obj.forEach) {
            obj.forEach(iterator,context);
        } else if (obj.length === +obj.length) {
            for (let i = 0, l = obj.length; i < l; i++) {
                iterator.call(context, obj[i], i, obj);
            }
        } else {
            for (let key in obj) {
                iterator.call(context, obj[key], key, obj);
            }
        }
    }

    //======================================================
    //  @method         invokeArray
    //  @usage          Invoke the named property on each
    //                  element of the array
    //  @params         {Array} arr - Objects Array
    //  @params         {String} property - Object Property
    //  @params         {Object} arg1 - Argument #1
    //  @params         {Object} arg2 - Argument #2
    //  @for            AppFramework
    //======================================================
    invokeArray(arr,property,arg1,arg2) {
        if (arr === null) { return; }
        for (let i = 0, l = arr.length; i < l; i++) {
            arr[i][property](arg1,arg2);
        }
    }

    //======================================================
    //  @method         detect
    //  @usage          Basic detection method, returns the
    //                  first instance where the iterator
    //                  returns truthy.
    //  @params         {Object} object - Object
    //  @params         {Object} iterator - Object Iterator
    //  @params         {Object} context - Object Context
    //  @params         {Object} arg1 - Argument #1
    //  @params         {Object} arg2 - Argument #2
    //  @returns        {Bool} result
    //  @for            AppFramework
    //======================================================
    detect(obj,iterator,context,arg1,arg2) {
        let result;
        if (obj === null) { return; }
        if (obj.length === +obj.length) {
            for (let i = 0, l = obj.length; i < l; i++) {
                result = iterator.call(context, obj[i], i, arg1,arg2);
                if(result) { return result; }
            }
            return false;
        } else {
            for (let key in obj) {
                result = iterator.call(context, obj[key], key, arg1,arg2);
                if(result) { return result; }
            }
            return false;
        }
    }

    //======================================================
    //  @method         getMap
    //  @usage          Returns a new Array with entries set
    //                  to the return value of the iterator.
    //  @params         {Object} object - Object
    //  @params         {Object} iterator - Object Iterator
    //  @params         {Object} context - Object Context
    //  @returns        {Object} result
    //  @for            AppFramework
    //======================================================
    getMap(obj, iterator, context) {
        let results = []; let _self = this;
        if (obj === null) { return results; }
        if (obj.map) { return obj.map(iterator, context); }
        _self.each(obj, function(value, index, list) {
            results[results.length] = iterator.call(context, value, index, list);
        });
        if (obj.length === +obj.length) { results.length = obj.length; }
        return results;
    }

    //======================================================
    //  @method         uniqArr
    //  @usage          Returns a sorted copy of unique
    //                  array elements with null removed
    //  @params         {Array} arr - Array
    //  @returns        {Array} arr - Unique Array
    //  @for            AppFramework
    //======================================================
    uniqArr(arr) {
        arr = arr.slice().sort();
        let output = [];
        let last = null;
        for(let i=0;i<arr.length;i++) {
            if(arr[i] !== void 0 && last !== arr[i]) {
                output.push(arr[i]);
            }
            last = arr[i];
        }
        return output;
    }

    //======================================================
    //  @method         shuffleArr
    //  @usage          Returns a new array with the same
    //                  entries as the source but in a
    //                  random order.
    //  @params         {Array} obj - Array
    //  @returns        {Array} arr - Suffled Array
    //  @for            AppFramework
    //======================================================
    shuffleArr(obj) {
        let shuffled = [], rand; let _self = this;
        _self.each(obj, function(value, index, list) {
            rand = Math.floor(Math.random() * (index + 1));
            shuffled[index] = shuffled[rand];
            shuffled[rand] = value;
        });
        return shuffled;
    }

    //======================================================
    //  @method         shuffleArr
    //  @usage          Return an object's keys as a new
    //                  Array
    //  @params         {Object} obj - Dest Object
    //  @returns        {Array} arr - Keys Array
    //  @for            AppFramework
    //======================================================
    keys(obj) {
        let _self = this;
        if(!_self.isObject(obj)) { throw "Invalid Object"; }
        let keys = [];
        for (let key in obj) { if (_self.hasProperty(obj, key)) { keys[keys.length] = key; } }
        return keys;
    }

    //======================================================
    //  @method         rangeArr
    //  @usage          Return an array in the range from
    //                  start to stop
    //  @returns        {Array} arr - New Array
    //  @for            AppFramework
    //======================================================
    rangeArr(start,stop,step) {
        step = step || 1;
        let len = Math.max(Math.ceil((stop - start) / step), 0);
        let idx = 0;
        let range = new Array(len);
        while(idx < len) {
            range[idx++] = start;
            start += step;
        }
        return range;
    }

    //======================================================
    //  @method         getUID
    //  @usage          Get Unique ID
    //  @returns        {Integer} uid - Unique ID
    //  @for            AppFramework
    //======================================================
    getUID() {
        let _self = this;
        _self.uniqueID += 1;
        return _self.uniqueID;
    }
}

//====================================================================
//  Base Class
//====================================================================
class AF_Base{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Base
    //======================================================
    constructor(settings = {}) {
        this.core = AppFramework.getInstance(); // Get Instance
        this.settings = (!this.core.isUndefined(settings) && this.core.isObject(settings))?settings:{};
    }

    //======================================================
    //  @method         setProperties
    //  @usage          Set Class Settings
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Base
    //======================================================
    setProperties(settings = {}){
        this.settings = (!this.core.isUndefined(settings) && this.core.isObject(settings))?settings:{};
        return this;
    }
}

//====================================================================
//  Evented Base Class
//====================================================================
class AF_Evented extends AF_Base{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Evented
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Add Listeners Object
        this.listeners = {};
    }

    //======================================================
    //  @method         addEvent
    //  @usage          Add Event
    //  @params         {String} name - Event Name
    //                  {Function} callback - Event Callback
    //  @for            AF_Evented
    //======================================================
    addEvent(name, callback = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to add event. Event name must be a string";
        if(!_self.core.isUndefined(callback) && !_self.core.isFunction(callback)) throw "Failed to add event. Event callback must be a function";

        // Check Listener
        if(_self.core.isUndefined(_self.listeners[name]) || !_self.core.isArray(_self.listeners[name])){
            _self.listeners[name] = []; // Create Array
        }
        _self.listeners[name].push({
            name: name,
            callback: callback
        });
    }

    //======================================================
    //  @method         removeEvent
    //  @usage          Remove Event
    //  @params         {String} name - Event Name
    //  @for            AF_Evented
    //======================================================
    removeEvent(name){
        let _self = this;
        if(!_self.core.isUndefined(_self.listeners[name])){
            _self.core.popProperty(_self.listeners, name); // Remove Event
            return true;
        }else{
            return false;
        }
    }

    //======================================================
    //  @method         triggerEvent
    //  @usage          Trigger Event
    //  @params         {String} name - Event Name
    //                  {Object} args - Event Arguments
    //  @for            AF_Evented
    //======================================================
    triggerEvent(name, args = {}){
        let _self = this;
        if(!_self.core.isUndefined(_self.listeners[name]) && _self.core.isArray(_self.listeners[name])){
            for(let i = 0; i < _self.listeners[name].length; i++){
                _self.listeners[name][i].callback(args);
            }
        }
    }

    //======================================================
    //  @method         removeAllEvents
    //  @usage          Remove All Events
    //  @for            AF_Evented
    //======================================================
    removeAllEvents(){
        let _self = this;
        let _keys = _self.core.keys(_self.listeners);
        if(_keys.length > 0){ // Has Keys
            for(let i = 0; i < _keys.length; i++){
                _self.core.popProperty(_self.listeners, _keys[i]);
            }
        }
    }
}

//====================================================================
//  Component Base Class
//====================================================================
class AF_Component extends AF_Evented{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Component
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Component Settings
        this.target = null;
        this.disabledHandlers = null; // Disabled Handlers
        this.isActive = true;
    }

    //======================================================
    //  @method         setTarget
    //  @usage          Set Component Target
    //  @params         {Object} target - Component Target
    //  @for            AF_Component
    //======================================================
    setTarget(target){
        this.target = target;
        return this;
    }

    //======================================================
    //  @method         setActive
    //  @usage          Set Component Active
    //  @params         {Bool} active - Status of Component
    //  @for            AF_Component
    //======================================================
    setActive(active){
        let _self = this;
        _self.isActive = active;
        if(_self.isActive){ // Is Active
            if(_self.disabledHandlers!=null && _self.core.isObject(_self.disabledHandlers)){
                _self.listeners = _self.disabledHandlers;
                _self.disabledHandlers = null;
            }
        }else{ // Not Active
            if(_self.listeners!=null && _self.core.isObject(_self.listeners) && _self.core.keys(_self.listeners).length > 0){
                _self.disabledHandlers = _self.listeners;
                _self.listeners = {};
            }
        }
        return this;
    }

    //======================================================
    //  @method         destroy
    //  @usage          Destroy Component
    //  @for            AF_Component
    //======================================================
    destroy(){
        let _self = this;
        _self.removeAllEvents(); // Remove All Events
        _self.disabledHandlers = null;
        return true;
    }
}

//====================================================================
//  Semicolon Game Object Base Class
//====================================================================
class AppModule extends AF_Evented{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AppModule
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Add Components Object
        this.components = {};
        this.disabledComponents = null; // Disabled Components
        this.isActive = true;
    }

    //======================================================
    //  @method         addComponent
    //  @usage          Add Component
    //  @params         {String} name - Component Name
    //  @params         {Class} class_name - Component Class
    //  @params         {Object} args - Component Arguments
    //  @params         {Callback} onComponentAdded - Component Added Callback
    //  @for            AppModule
    //======================================================
    addComponent(name, class_name = SE_Base, args = {}, onComponentAdded = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to add component. Component name must be a string";
        if(!_self.core.isUndefined(onComponentAdded) && !_self.core.isFunction(onComponentAdded)) throw "Failed to add component. Component callback must be a function";

        // Check Listener
        if(_self.core.isUndefined(_self.components[name])){
            _self.components[name] = new class_name(args);
            _self.components[name].setTarget(_self);
            _self.components[name].setActive(true);
            onComponentAdded();
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         removeComponent
    //  @usage          Remove Component
    //  @params         {String} name - Component Name
    //  @params         {Callback} onComponentRemoved - Component Removed Callback
    //  @for            AppModule
    //======================================================
    removeComponent(name, onComponentRemoved = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to remove component. Component name must be a string";
        if(!_self.core.isUndefined(_self.components[name])){
            _self.components[name].destroy(); // Destroy
            _self.core.popProperty(_self.components, name);
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         getComponent
    //  @usage          Get Component
    //  @params         {String} name - Component Name
    //  @returns        {Object} instace - Component Instance
    //  @for            AppModule
    //======================================================
    getComponent(name){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to get component. Component name must be a string";
        if(!_self.core.isUndefined(_self.components[name])){
            return _self.components[name];
        }else{
            return null;
        }
    }

    //======================================================
    //  @method         setActive
    //  @usage          Set AppModule Active
    //  @params         {Bool} active - Status of Game Object
    //  @for            AppModule
    //======================================================
    setActive(active){
        let _self = this;
        _self.isActive = active;
        if(active){ // Active Game Object
            if(_self.disabledComponents!=null && _self.core.isObject(_self.disabledComponents)){
                _self.components = _self.disabledComponents;
                _self.disabledComponents = null;
                let _keys = _self.core.keys(_self.components);
                for(let i = 0; i < _keys.length; i++){
                    _self.components[_keys[i]].setActive(true);
                }
            }
        }else{ // Not Active
            let _keys = _self.core.keys(_self.components);
            if(_self.components!=null && _self.core.isObject(_self.components) && _keys.length > 0){
                for(let i = 0; i < _keys.length; i++){
                    _self.components[_keys[i]].setActive(false);
                }

                _self.disabledComponents = _self.components;
                _self.components = {};
            }
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         destroy
    //  @usage          Destroy AppModule
    //  @for            AppModule
    //======================================================
    destroy(){
        let _self = this;
        _self.removeAllEvents(); // Remove All Events
        _self.disabledComponents = null;
        let _keys = _self.core.keys(_self.components);
        for(let i = 0; i < _keys.length; i++){
            _self.components[_keys[i]].destroy();
        }
        return true;
    }

    //******************************************************
    //  Game Object Utils Methods
    //******************************************************
    //======================================================
    //  @method         setProperty
    //  @usage          Set AppModule Properties with Event
    //  @params         {String} name - Property Name
    //  @params         {Var} value - Property Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            AppModule
    //======================================================
    setProperty(name, value){
        if(this.core.isUndefined(this.settings[name])) throw "Failed to set AppModule property. Property with name \""+name+"\" is not found.";
        this.settings[name] = value;
        this.triggerEvent("onPropertyChanged", {
            name: name,
            value: value
        });
        return this;
    }

    //======================================================
    //  @method         getProperty
    //  @usage          Get AppModule Property
    //  @params         {String} name - Property Name
    //  @returns        {Var} value - Property Value
    //  @for            AppModule
    //======================================================
    getProperty(name){
        return this.settings[name];
    }

    //======================================================
    //  @method         addToProperty
    //  @usage          Add value to AppModule Property with Event
    //  @params         {String} name - Property Name
    //  @params         {String} value - Inc Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            AppModule
    //======================================================
    addToProperty(name, addValue = 1){
        let _last = this.getProperty(name);
        if(this.core.isUndefined(_last) || _last === null) throw "Failed to add AppModule property value. Property with name \""+name+"\" is not found.";
        _last += addValue; // Add to Value
        this.setProperty(name, _last);
        return this;
    }

    //======================================================
    //  @method         removeFromProperty
    //  @usage          Remove value to AppModule Property with Event
    //  @params         {String} name - Property Name
    //  @params         {Int} removeNumber - Dec Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            GameObject
    //======================================================
    removeFromProperty(name, removeNumber = 1){
        let _last = this.getProperty(name);
        if(this.core.isUndefined(_last) || _last === null) throw "Failed to add AppModule property value. Property with name \""+name+"\" is not found.";
        _last -= removeNumber; // Add to Value
        this.setProperty(name, _last);
        return this;
    }

    //======================================================
    //  @method         serializeObjectData
    //  @usage          Serialize AppModule and Return Data
    //  @returns        {Var} Value - Serialized Object Data
    //  @for            GameObject
    //======================================================
    serializeObjectData(){
        let _data = this.settings, _new_data; // Get Settings
        _new_data = JSON.stringify(_data); // Serialize
        return _new_data;
    }

    //======================================================
    //  @method         deserealizeObjectData
    //  @usage          Deserialize AppModule and Return Data
    //  @params         {Var} data - Data to Deserialize
    //  @params         {Bool} auto_load - Autoload to Props?
    //  @returns        {Object} Value - Deserialized Object Data
    //  @for            GameObject
    //======================================================
    deserealizeObjectData(data = "", auto_load = true){
        if(!this.core.isString(data)) throw "Failed to deserialize AppModule data. Data must be a string.";
        let _data = JSON.parse(data); // Parse
        if(auto_load) this.setProperties(_data); // Set Properties
        return _data; // Return Data
    }
}