# JS Component System
This is simple Component System Framework for JavaScript (ES6). This framework can work with Components, Evented Classes and Modules.

<b>The Component System has:</b>
- General (Core) Class;
- Base Class;
- Evented Class;
- Component Base Class;
- AppModule Base Class (like a GameObject);

## Get Started
<b>What is it?</b><br/>
<b>JS Component System</b> - a simple implementation of evented objects with component support for your JS project (ES6).<br/><br/>
Every Object (in this case it's named as Modules) has multiple events and components support.

<b>Download JS Component system using GIT.Clone:</b><br/>
`https://gitlab.com/ocugine_games/js-component-system.git`

<b>Inlcude script to end of document body:</b><br/>
`<script src="path_to_script/js_component.js"></script>`

<b>Create your Application Instance:</b><br/>
```javascript
AppFramework.onReady(function(){
    // Initialize App Framework
    let APP = new AppFramework({
        /* Here you can pass any application args. From object you can get them by "this.core.settings" */
    }, true);
});
```

<b>Done.</b><br/>
Now you can work with your application using JavaScript Component System.

## Create New Modules
Now, lets overview how you can create a new module for your application.<br/>
Just create your own module class extends from <b>AppModule</b> like here:
```javascript
// Create your Module from AppModule class
class ExampleModule extends AppModule{
    // Module Constructor with default properties args
    constructor(settings = {}) {
        super(settings);
    }

    // Module Method
    testMethod(argument){
        /* DO Something
        return this; // Return Module Instance
    }
}
```

So, your first module is ready!<br/>
Every module has some methods from base <b>AppModule</b> class:

| Method | Arguments | Usage |
| --- | --- | --- |
| **setProperties**`public` | settings`object` | Set Module Properties |
| **setProperty**`public` | name`string`<br/>value`var` | Set AppModule Property value by Name and Trigger event `onPropertyChanged` |
| **getProperty**`public` | name`string` | Get AppModule Property by Name |
| **addToProperty**`public` | name`string`<br/>addValue`var` | Add value (increment) to AppModule Property with Trigger event `onPropertyChanged` |
| **removeFromProperty**`public` | name`string`<br/>removeNumber`var` | Add value (decrement) to AppModule Property with Trigger event `onPropertyChanged` |
| **addEvent**`public` | name`string`<br/>callback`function` | Add Event by Name |
| **removeEvent**`public` | name`string` | Remove Event by Name |
| **triggerEvent**`public` | name`string`<br/>args`object` | Trigger added Event by Name |
| **removeAllEvents**`public` | - | Remove All Events for this Module (Not for Child components) |
| **addComponent**`public` | name`string`<br/>class_name`class`<br/>args`object`<br/>onComponentAdded`function` | Add Component |
| **removeComponent**`public` | name`string`<br/>onComponentRemoved`function` | Remove Component by Name |
| **getComponent**`public` | name`string` | Get Component by Name |
| **setActive**`public` | active`bool` | Set AppModule Active. If disabled - mute all object listeners and all child component listeners. |
| **serializeObjectData**`public` | - | Serialize AppModule with JSON and Return Data |
| **deserealizeObjectData**`public` | data`string`<br/>auto_load`bool` | Deserialize AppModule Data from JSON and Return Object |
| **destroy**`public` | | Destroy AppModule. It means - remove all events, disable all components. |

## Load Modules to the Core Class
<b>If you need to load modules into Application Core class you can use this method:</b>
```javascript
// Load module to your app
APP.loadModule("MODULE_NAME", MODULE_CLASS, {
    /* Module Settings */
});
```

<b>And you can get access to this module by:</b>
```javascript
// Get Access to the module instance
let _module = APP.module("MODULE_NAME");
```

<b>Or you can find module by class:</b>
```javascript
// Get Access to the module instance
let _module = APP.getModuleByType(MODULE_CLASS);
```

<b>Access to loaded modules from other module:</b>
```javascript
MyModuleMethod(){
    let _module = this.core.module("MODULE_NAME"); // Access to core
}
```

## Core Class Reference
You can get access to every AppFramework methods and params from any JS Component system object using:
```javascript
this.core.METHOD_NAME(); // Call Method
```

Here you can see a short AppFramework API Reference:

| Method | Arguments | Usage |
| --- | --- | --- |
**getInstance**`static` | - | Get AppFramework Instance
**loadModule**`public` | name`string`<br/>class_name`class`<br/>args`object` | Load Module using name and Class Reference to AppFramework Instance |
**module**`public` | name`string` | Get module Instance by Name. If instance not found - **returns false** |
**getModuleByType**`public` | class_name`class` | Get module Instance by Class. If instance not found - **returns false** |
**log**`public` | text`string` | Beauty Alternative to the Console.log. Works only if **AppFramework.debug = false** |
**onReady**`static` | callback`function` | Alternative to the **Document ready** function from jQuery |
**configure**`public` | settings`object` | Set **AppFramework.settings** for this instance |
**normalizeArg**`system` | args`string` | Convert Arguments from **comma-separated** string to **array**. |
**extendObject**`system` | dest`object`<br/>source`object` | Extend **Destination** object by Source **object** |
**cloneObject**`system` | dest`object` | Clone **Destination** object |
**hasProperty**`system` | obj`object`<br/>key`string` | Looking for property with **key** in the **obj** |
**isString**`internal` | obj`var` | Check if something is string |
**isNumber**`internal` | obj`var` | Check if something is number |
**isFunction**`internal` | obj`var` | Check if something is function |
**isObject**`internal` | obj`var` | Check if something is object |
**isArray**`internal` | obj`var` | Check if something is array |
**isUndefined**`internal` | obj`var` | Check if something is undefined |
**popProperty**`system` | obj`object`<br/>property`string` | Remove property from object |
**each**`system` | obj`object`<br/>iterator`object`<br/>context`object` | Fast Basic Iteration Method |
**invokeArray**`system` | arr`array`<br/>property`name`<br/>arg1`var`<br/>arg2`var` | Invoke the named property on each element of the array |
**detect**`system` | obj`object`<br/>iterator`object`<br/>context`object`<br/>arg1`var`<br/>arg2`var` | Basic detection method, returns the first instance where the iterator returns truthy. |
**getMap**`system` | obj`object`<br/>iterator`object`<br/>context`object` | Returns a new Array with entries set to the return value of the iterator. |
**uniqArr**`system` | arr`array` | Returns a sorted copy of unique array elements with null removed |
**shuffleArr**`system` | arr`array` | Returns a new array with the same entries as the source but in a random order. |
**keys**`system` | obj`object` | Return an object's keys as a new array |
**rangeArr**`system` | start`int`<br/>stop`int`<br/>step`int` | Return an array in the range from start to stop |
**getUID**`system` | - | Get Unique ID for everything |

## Create New Components
For every Module you can create unlimited components with events support.<br/>
Just create your own component class extends from <b>AF_Component</b> like here:
```javascript
class ExampleComponent extends AF_Component{
    // Module Constructor
    constructor(settings = {}) {
        super(settings);
    }

    /* TODO: Your Methods */
}
```

So, your first component is ready!<br/>
<b>And now you can add this component to your Module:</b>
```javascript
MYMODULE.addComponent("MY_COMPONENT", MYCOMPONENT, {
    /* Component Settings */
}, function(){
    /* Component Added */
});
```

<b>Or you can remove this component:</b>
```javascript
MYCOMPONENT.destroy();
delete MYCOMPONENT;
```

Also you can disable / enable components. When you disable a component - all listeners of this component will be disabled.<br/>
<b>For Example:</b>
```javascript
MYCOMPONENT.setActive(true);
```

Every component has some methods from base <b>AF_Component</b> class:

| Method | Arguments | Usage |
| --- | --- | --- |
| **setProperties**`public` | settings`object` | Set Component Properties |
| **setTarget**`internal` | target`object` | Set target module instance for this component instance |
| **addEvent**`public` | name`string`<br/>callback`function` | Add Event by Name |
| **removeEvent**`public` | name`string` | Remove Event by Name |
| **triggerEvent**`public` | name`string`<br/>args`object` | Trigger added Event by Name |
| **removeAllEvents**`public` | - | Remove All Events for this Component |
| **setActive**`public` | active`bool` | Set Component Active. If disabled - mute all Component listeners. |
| **destroy**`public` | | Destroy Component. It means - remove all events for this component. |

## Create Other Classes
You can create your own classed from AF_Base class without events and components support:<br/>
```javascript
class SimpleExample extends AF_Base{
    // Class Constructor
    constructor(settings = {}) {
        super(settings);
    }

    /* TODO: Your Methods */
}
```

Every class has some methods from base <b>AF_Base</b> class:

| Method | Arguments | Usage |
| --- | --- | --- |
| **setProperties**`public` | settings`object` | Set Component Properties |

<b>Or create a new class with Events support but without components:</b>
```javascript
class EventedExample extends AF_Evented{
    // Class Constructor
    constructor(settings = {}) {
        super(settings);
    }

    /* TODO: Your Methods */
}
```

Every component has some methods from base <b>AF_Evented</b> class:

| Method | Arguments | Usage |
| --- | --- | --- |
| **setProperties**`public` | settings`object` | Set Component Properties |
| **addEvent**`public` | name`string`<br/>callback`function` | Add Event by Name |
| **removeEvent**`public` | name`string` | Remove Event by Name |
| **triggerEvent**`public` | name`string`<br/>args`object` | Trigger added Event by Name |
| **removeAllEvents**`public` | - | Remove All Events for this Component |


## Help
You can contact me if you need to get support with this system.<br/>
<b>Just email me:</b> <a href="mailto:help@cdbits.net">help@cdbits.net</a>