//====================================================================
//  JavaScript Component System
//====================================================================
//  (c) 2020 Ocugine Games. Developed by Elijah Rastorguev
//  Semicolon Engine may be freely distributed under the MIT license or
//  GPLv2 License.
//
//  For all details and documentation:
//  https://gitlab.com/ocugine_games/js-component-system/
//
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Component Base Class
//====================================================================
class AF_Component extends AF_Evented{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Component
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Component Settings
        this.target = null;
        this.disabledHandlers = null; // Disabled Handlers
        this.isActive = true;
    }

    //======================================================
    //  @method         setTarget
    //  @usage          Set Component Target
    //  @params         {Object} target - Component Target
    //  @for            AF_Component
    //======================================================
    setTarget(target){
        this.target = target;
        return this;
    }

    //======================================================
    //  @method         setActive
    //  @usage          Set Component Active
    //  @params         {Bool} active - Status of Component
    //  @for            AF_Component
    //======================================================
    setActive(active){
        let _self = this;
        _self.isActive = active;
        if(_self.isActive){ // Is Active
            if(_self.disabledHandlers!=null && _self.core.isObject(_self.disabledHandlers)){
                _self.listeners = _self.disabledHandlers;
                _self.disabledHandlers = null;
            }
        }else{ // Not Active
            if(_self.listeners!=null && _self.core.isObject(_self.listeners) && _self.core.keys(_self.listeners).length > 0){
                _self.disabledHandlers = _self.listeners;
                _self.listeners = {};
            }
        }
        return this;
    }

    //======================================================
    //  @method         destroy
    //  @usage          Destroy Component
    //  @for            AF_Component
    //======================================================
    destroy(){
        let _self = this;
        _self.removeAllEvents(); // Remove All Events
        _self.disabledHandlers = null;
        return true;
    }
}