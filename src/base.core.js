//====================================================================
//  JavaScript Component System
//====================================================================
//  (c) 2020 Ocugine Games. Developed by Elijah Rastorguev
//  Semicolon Engine may be freely distributed under the MIT license or
//  GPLv2 License.
//
//  For all details and documentation:
//  https://gitlab.com/ocugine_games/js-component-system/
//
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Base Class
//====================================================================
class AF_Base{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Base
    //======================================================
    constructor(settings = {}) {
        this.core = AppFramework.getInstance(); // Get Instance
        this.settings = (!this.core.isUndefined(settings) && this.core.isObject(settings))?settings:{};
    }

    //======================================================
    //  @method         setProperties
    //  @usage          Set Class Settings
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Base
    //======================================================
    setProperties(settings = {}){
        this.settings = (!this.core.isUndefined(settings) && this.core.isObject(settings))?settings:{};
        return this;
    }
}