//====================================================================
//  JavaScript Component System
//====================================================================
//  (c) 2020 Ocugine Games. Developed by Elijah Rastorguev
//  Semicolon Engine may be freely distributed under the MIT license or
//  GPLv2 License.
//
//  For all details and documentation:
//  https://gitlab.com/ocugine_games/js-component-system/
//
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Semicolon Game Object Base Class
//====================================================================
class AppModule extends AF_Evented{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AppModule
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Add Components Object
        this.components = {};
        this.disabledComponents = null; // Disabled Components
        this.isActive = true;
    }

    //======================================================
    //  @method         addComponent
    //  @usage          Add Component
    //  @params         {String} name - Component Name
    //  @params         {Class} class_name - Component Class
    //  @params         {Object} args - Component Arguments
    //  @params         {Callback} onComponentAdded - Component Added Callback
    //  @for            AppModule
    //======================================================
    addComponent(name, class_name = SE_Base, args = {}, onComponentAdded = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to add component. Component name must be a string";
        if(!_self.core.isUndefined(onComponentAdded) && !_self.core.isFunction(onComponentAdded)) throw "Failed to add component. Component callback must be a function";

        // Check Listener
        if(_self.core.isUndefined(_self.components[name])){
            _self.components[name] = new class_name(args);
            _self.components[name].setTarget(_self);
            _self.components[name].setActive(true);
            onComponentAdded();
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         removeComponent
    //  @usage          Remove Component
    //  @params         {String} name - Component Name
    //  @params         {Callback} onComponentRemoved - Component Removed Callback
    //  @for            AppModule
    //======================================================
    removeComponent(name, onComponentRemoved = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to remove component. Component name must be a string";
        if(!_self.core.isUndefined(_self.components[name])){
            _self.components[name].destroy(); // Destroy
            _self.core.popProperty(_self.components, name);
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         getComponent
    //  @usage          Get Component
    //  @params         {String} name - Component Name
    //  @returns        {Object} instace - Component Instance
    //  @for            AppModule
    //======================================================
    getComponent(name){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to get component. Component name must be a string";
        if(!_self.core.isUndefined(_self.components[name])){
            return _self.components[name];
        }else{
            return null;
        }
    }

    //======================================================
    //  @method         setActive
    //  @usage          Set AppModule Active
    //  @params         {Bool} active - Status of Game Object
    //  @for            AppModule
    //======================================================
    setActive(active){
        let _self = this;
        _self.isActive = active;
        if(active){ // Active Game Object
            if(_self.disabledComponents!=null && _self.core.isObject(_self.disabledComponents)){
                _self.components = _self.disabledComponents;
                _self.disabledComponents = null;
                let _keys = _self.core.keys(_self.components);
                for(let i = 0; i < _keys.length; i++){
                    _self.components[_keys[i]].setActive(true);
                }
            }
        }else{ // Not Active
            let _keys = _self.core.keys(_self.components);
            if(_self.components!=null && _self.core.isObject(_self.components) && _keys.length > 0){
                for(let i = 0; i < _keys.length; i++){
                    _self.components[_keys[i]].setActive(false);
                }

                _self.disabledComponents = _self.components;
                _self.components = {};
            }
        }

        // Return Game Object
        return this;
    }

    //======================================================
    //  @method         destroy
    //  @usage          Destroy AppModule
    //  @for            AppModule
    //======================================================
    destroy(){
        let _self = this;
        _self.removeAllEvents(); // Remove All Events
        _self.disabledComponents = null;
        let _keys = _self.core.keys(_self.components);
        for(let i = 0; i < _keys.length; i++){
            _self.components[_keys[i]].destroy();
        }
        return true;
    }

    //******************************************************
    //  Game Object Utils Methods
    //******************************************************
    //======================================================
    //  @method         setProperty
    //  @usage          Set AppModule Properties with Event
    //  @params         {String} name - Property Name
    //  @params         {Var} value - Property Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            AppModule
    //======================================================
    setProperty(name, value){
        if(this.core.isUndefined(this.settings[name])) throw "Failed to set AppModule property. Property with name \""+name+"\" is not found.";
        this.settings[name] = value;
        this.triggerEvent("onPropertyChanged", {
            name: name,
            value: value
        });
        return this;
    }

    //======================================================
    //  @method         getProperty
    //  @usage          Get AppModule Property
    //  @params         {String} name - Property Name
    //  @returns        {Var} value - Property Value
    //  @for            AppModule
    //======================================================
    getProperty(name){
        return this.settings[name];
    }

    //======================================================
    //  @method         addToProperty
    //  @usage          Add value to AppModule Property with Event
    //  @params         {String} name - Property Name
    //  @params         {String} value - Inc Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            AppModule
    //======================================================
    addToProperty(name, addValue = 1){
        let _last = this.getProperty(name);
        if(this.core.isUndefined(_last) || _last === null) throw "Failed to add AppModule property value. Property with name \""+name+"\" is not found.";
        _last += addValue; // Add to Value
        this.setProperty(name, _last);
        return this;
    }

    //======================================================
    //  @method         removeFromProperty
    //  @usage          Remove value to AppModule Property with Event
    //  @params         {String} name - Property Name
    //  @params         {Int} removeNumber - Dec Value
    //  @returns        {Object} instance - GameObject Instance
    //  @for            GameObject
    //======================================================
    removeFromProperty(name, removeNumber = 1){
        let _last = this.getProperty(name);
        if(this.core.isUndefined(_last) || _last === null) throw "Failed to add AppModule property value. Property with name \""+name+"\" is not found.";
        _last -= removeNumber; // Add to Value
        this.setProperty(name, _last);
        return this;
    }

    //======================================================
    //  @method         serializeObjectData
    //  @usage          Serialize AppModule and Return Data
    //  @returns        {Var} Value - Serialized Object Data
    //  @for            GameObject
    //======================================================
    serializeObjectData(){
        let _data = this.settings, _new_data; // Get Settings
        _new_data = JSON.stringify(_data); // Serialize
        return _new_data;
    }

    //======================================================
    //  @method         deserealizeObjectData
    //  @usage          Deserialize AppModule and Return Data
    //  @params         {Var} data - Data to Deserialize
    //  @params         {Bool} auto_load - Autoload to Props?
    //  @returns        {Object} Value - Deserialized Object Data
    //  @for            GameObject
    //======================================================
    deserealizeObjectData(data = "", auto_load = true){
        if(!this.core.isString(data)) throw "Failed to deserialize AppModule data. Data must be a string.";
        let _data = JSON.parse(data); // Parse
        if(auto_load) this.setProperties(_data); // Set Properties
        return _data; // Return Data
    }
}