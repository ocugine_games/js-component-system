//====================================================================
//  JavaScript Component System
//====================================================================
//  (c) 2020 Ocugine Games. Developed by Elijah Rastorguev
//  Semicolon Engine may be freely distributed under the MIT license or
//  GPLv2 License.
//
//  For all details and documentation:
//  https://gitlab.com/ocugine_games/js-component-system/
//
//  @version            0.5.0
//  @build              502
//  @developer          Ocugine Games
//====================================================================
//====================================================================
//  Evented Base Class
//====================================================================
class AF_Evented extends AF_Base{
    //======================================================
    //  @method         constructor
    //  @usage          Class Constructor
    //  @params         {Object} settings - Class Settings
    //  @for            AF_Evented
    //======================================================
    constructor(settings = {}) {
        super(settings);

        // Add Listeners Object
        this.listeners = {};
    }

    //======================================================
    //  @method         addEvent
    //  @usage          Add Event
    //  @params         {String} name - Event Name
    //                  {Function} callback - Event Callback
    //  @for            AF_Evented
    //======================================================
    addEvent(name, callback = function(){}){
        // Check Params
        let _self = this;
        if(_self.core.isUndefined(name) || !_self.core.isString(name)) throw "Failed to add event. Event name must be a string";
        if(!_self.core.isUndefined(callback) && !_self.core.isFunction(callback)) throw "Failed to add event. Event callback must be a function";

        // Check Listener
        if(_self.core.isUndefined(_self.listeners[name]) || !_self.core.isArray(_self.listeners[name])){
            _self.listeners[name] = []; // Create Array
        }
        _self.listeners[name].push({
            name: name,
            callback: callback
        });
    }

    //======================================================
    //  @method         removeEvent
    //  @usage          Remove Event
    //  @params         {String} name - Event Name
    //  @for            AF_Evented
    //======================================================
    removeEvent(name){
        let _self = this;
        if(!_self.core.isUndefined(_self.listeners[name])){
            _self.core.popProperty(_self.listeners, name); // Remove Event
            return true;
        }else{
            return false;
        }
    }

    //======================================================
    //  @method         triggerEvent
    //  @usage          Trigger Event
    //  @params         {String} name - Event Name
    //                  {Object} args - Event Arguments
    //  @for            AF_Evented
    //======================================================
    triggerEvent(name, args = {}){
        let _self = this;
        if(!_self.core.isUndefined(_self.listeners[name]) && _self.core.isArray(_self.listeners[name])){
            for(let i = 0; i < _self.listeners[name].length; i++){
                _self.listeners[name][i].callback(args);
            }
        }
    }

    //======================================================
    //  @method         removeAllEvents
    //  @usage          Remove All Events
    //  @for            AF_Evented
    //======================================================
    removeAllEvents(){
        let _self = this;
        let _keys = _self.core.keys(_self.listeners);
        if(_keys.length > 0){ // Has Keys
            for(let i = 0; i < _keys.length; i++){
                _self.core.popProperty(_self.listeners, _keys[i]);
            }
        }
    }
}